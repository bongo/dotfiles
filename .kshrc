# greeting
unix

# aliases
alias ls="colorls -agG"
alias add="doas pkg_add"
alias del="doas pkg_delete"
alias sr="pkg_info -Q"
alias sysup="doas pkg_add -u"
alias poweroff="doas poweroff"
alias cp="cp -r"
alias vi="vise"
alias vim="vise"
alias vis="vise" 
alias lsblk="doas lsblk"
alias temp="sysctl hw.sensors.cpu0.temp0"
alias du="du -h"
alias df="df -h"
alias free="free -h"
alias q="exit"
alias azan="azan -a"

# emacs mode
set -o emacs

# ctrl + l for clearing screen
bind -m '^L'='^U'clear'^J''^Y'

# bash prompt
if ! test "$(id -u)" -eq 0; then
	PS1='[\u@\h \W]$ '
else
	PS1='[\u@\h \W]# '
fi

# powerlevel10k-like prompt
#function __git_ps1 {
#    git branch 2> /dev/null | sed 's/*//g'
#}
#function bc {
#    echo "\\\\033[48;5;"$1"m\\"
#}
#
#function fc {
#    echo "\\\\033[38;5;"$1"m\\"
#}
#
#export distro_logo=""
#
#prompt_command() {
#    case $err in
#        0) printf "$(bc 252)$(fc 238) $distro_logo $(bc 238)$(fc 252) $(fc 2)$(pwd | sed "s|^$HOME|~|")$(fc 6) $(__git_ps1) $(fc 34) \033[0m$(fc 238)\033[m " ;;
#        *) printf "$(bc 252)$(fc 238) $distro_logo $(bc 238)$(fc 252) $(fc 2)$(pwd | sed "s|^$HOME|~|")$(fc 6) $(__git_ps1) $(fc 1) $err \033[m$(fc 238)\033[m " ;;
#    esac
#}
#
#PS1='$(err=$? prompt_command 2>/dev/null)'
