export LANG=en_US.UTF-8
export ENV=$HOME/.kshrc
export PATH=$HOME/.local/bin:$PATH
export EDITOR=vis
export FCEDIT=vis
export VISUAL=vis
export PAGER=less
export HISTFILE=$HOME/.ksh_history

command -v sx || command -v startx
