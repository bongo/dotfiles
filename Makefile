PREFIX = ${HOME}/.local
CONF = ${HOME}/.config
GITSITE = "https://codeberg.org/bongo"
ROOTCOMMAND = $(shell command -v sudo || command -v doas)

all: configs scripts fonts dwm

configs:
	mkdir -p ${CONF}
	cp -r .config/* ${CONF}/

scripts:
	mkdir -p ${PREFIX}/bin/
	cp -r .local/bin/* ${PREFIX}/bin/

fonts:
	mkdir -p ${PREFIX}/share/fonts
	cp -r .local/share/fonts/* ${PREFIX}/share/fonts

dwm:
	[ -d src/dwm ] && cd src/dwm && git pull || git clone ${GITSITE}/dwm src/dwm
	[ -d src/slstatus ] && cd src/slstatus && git pull || git clone ${GITSITE}/slstatus src/slstatus
	[ -d src/st ] && cd src/st && git pull || git clone ${GITSITE}/st src/st
	cd src/dwm && ./configure && make && ${ROOTCOMMAND} make install
	cd src/slstatus && ./configure && make && ${ROOTCOMMAND} make install
	cd src/st && ./configure && make && ${ROOTCOMMAND} make install

