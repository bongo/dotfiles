# dotfiles
these are my own dotfiles, feel free to do whatever the fuck you want with it

## required:
- qutebrowser
- sx (linux only)
- sxhkd
- mpv
- vis
- scrot
- xclip
- slock
- [azan](https://github.com/afify/azan)
- oksh

## qutebrowser config features:
* cool gruvbox theme
* jmatrix (python uMatrix implementation for qutebrowser)
* privacy configurations
* privacy front-end redirections (i.e. youtube to invidious)

## installation:
running `make` copies the config files to their own directories and installs my own suckless software builds

## notes:
the aliases in .kshrc expect you to be using openbsd
