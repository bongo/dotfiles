// ==UserScript==
// @name     _Remap links to https
// @include  http://YOUR_SERVER.COM/YOUR_PATH/*
// @require  http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js
// @require  https://gist.github.com/raw/2625891/waitForKeyElements.js
// @grant    GM_addStyle
// ==/UserScript==
/*- The @grant directive is needed to work around a design change
    introduced in GM 1.0.   It restores the sandbox.
*/
waitForKeyElements ("a", remapToSSL);

function remapToSSL (jNode) {
    var node    = jNode.get (0);

    if (node.protocol === "http:") {
        node.protocol = "https:";
    }
